package leapyear;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LeapYearCheckerTests {
	LeapYearChecker checker = new LeapYearChecker();
	
	@Test
	public void yearDivisibleBy4IsLeapYear() {
		boolean isLeapYear = checker.isLeapYear(1996);
		assertTrue(isLeapYear);
	}
	
	@Test
	public void yearNotDivisibleBy4IsNotLeapYear() {
		boolean isLeapYear = checker.isLeapYear(1997);
		assertFalse(isLeapYear);
	}
	
	@Test
	public void yearDivisibleBy100IsNotLeapYear() {
		boolean isLeapYear = checker.isLeapYear(1900);
		assertFalse(isLeapYear);
	}
	
	@Test
	public void yearDivisibleBy400IsLeapYear() {
		boolean isLeapYear = checker.isLeapYear(1600);
		assertTrue(isLeapYear);
	}
}
