package stepDefinitions;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GoogleSearchStepDef {
	ChromeDriver driver;
	
	@Before
	public void setUp() {
		driver = new ChromeDriver();
	}
	
	@Given("I navigate to the Google home page")
	public void i_navigate_to_the_Google_home_page() {
	    driver.get("https://www.google.com/");
	}

	@When("I type Coke in the search bar")
	public void i_type_Coke_in_the_search_bar() {
		WebElement searchBox = driver.findElement(By.name("q"));
		searchBox.sendKeys("coke");
		searchBox.sendKeys(Keys.RETURN);
	}

	@Then("I see our site in the search results")
	public void i_see_our_site_in_the_search_results() {
		String pageText = driver.findElement(By.tagName("body")).getText();
		assertTrue(pageText.contains("coca-cola.com"));
	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
}
