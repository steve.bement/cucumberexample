package stepDefinitions;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import leapyear.LeapYearChecker;

public class LeapYearStepDef {
	int year;
	LeapYearChecker checker;
	boolean result;
	
	@Before
	public void before() {
		checker = new LeapYearChecker();
	}
	
	@Given("the year {int}")
	public void the_year(int year) {
	    this.year = year;
	}

	@When("I invoke the checker")
	public void i_invoke_the_checker() {
	    result = checker.isLeapYear(year);
	}

	@Then("the result will be true")
	public void the_result_will_be_true() {
	    assertTrue(result);
	}

	@Then("the result will be false")
	public void the_result_will_be_false() {
	    assertFalse(result);
	}
}
