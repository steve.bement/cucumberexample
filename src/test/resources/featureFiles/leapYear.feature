Feature: Leap Year Checker

  Scenario: Year divisible by 4 is leap year
    Given the year 1996
    When I invoke the checker
    Then the result will be true

  Scenario: Year not divisible by 4 is not leap year
    Given the year 1997
    When I invoke the checker
    Then the result will be false

  Scenario: Year divisible by 100 is not leap year
    Given the year 1900
    When I invoke the checker
    Then the result will be false

  Scenario: Year divisible by 400 is leap year
    Given the year 1600
    When I invoke the checker
    Then the result will be true
