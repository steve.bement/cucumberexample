Feature: Google Search

  Scenario: Search for Coke
    Given I navigate to the Google home page
    When I type Coke in the search bar
    Then I see our site in the search results
